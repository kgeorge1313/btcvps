FROM python:3.7
RUN pip install --upgrade pip
RUN mkdir /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN cat requirements.txt | xargs -n 1 pip install || echo "Failed installing some packages!"
CMD ["python", "manage.py",  "runserver", "0.0.0.0:8080"]