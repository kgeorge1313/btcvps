function VPSconfigFormPlan() {
    var div = document.getElementById("select_vps_plan_div");
    var sel = document.createElement('select');
    sel.className = "uk-select";
    const vultr_plans_list_url = "https://cors-anywhere.herokuapp.com/https://api.vultr.com/v1/plans/list";
    options = "";
    $.get(vultr_plans_list_url, function (data, status) {
        var sorting = {};

        Object.keys(data).forEach(function (key) {
            sorting[data[key]['price_per_month']] = key
        });
        console.log(sorting.sortByKey());
        Object.keys(sorting).forEach(function (key) {
            console.log(key);
            options += ("<option>" + data[sorting[key]]["name"] + "</option>");
        });
        console.log(options);
        sel.innerHTML = options;
        div.appendChild(sel);
    });
}

export {VPSconfigFormPlan}